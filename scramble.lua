--[[
   This widget adds a new order "scramble". This is an optiontal area command.
   If an area is given, then the units will scramble randomly around that area
   until a new order is given. If the radius == 0 (player just clicked with no
   drag), then the units will "scramble" their way to that point (pathing
   randomly to that position).

   Constructors, rezbots and aircraft will use the "fight" command when
   scrambling to make repairs and reclaim wrecks.

   This command is useful to have units hold a line, but avoid standing still or
   moving in straight lines to avoid artillery fire.

   It is also useful for t1 spam. Set the factory to scramble to an area behind
   the enemy lines and they'll take a randomized path there. This helps to avoid
   crowd control measures and junos and gets the spam into every nook and cranny
   of the base.
]]

function widget:GetInfo()
  return {
    name = "Scramble",
    desc = "Adds a command to have units scramble around an area.",
    author = "UnicodeSnowman",
    date = "Jul 18, 2023",
    license = "GNU GPL, v2 or later",
    handler = true,
    layer = 50000,
    enabled = true  --  loaded by default?
  }
end

-- Memoizes pure functions for performance benefits.
local function memoize(fn)
  local memo_table = {}
  return function (arg)
    local ret = memo_table[arg]
    if ret then
      return ret
    end
    ret = fn(arg)
    memo_table[arg] = ret
    return ret
  end
end

local UnitDefs = assert(UnitDefs)

local function dump(o)
  if type(o) == 'table' then
    local s = '{ '
    for k,v in pairs(o) do
      if type(k) ~= 'number' then k = '"'..k..'"' end
      s = s .. '['..k..'] = ' .. dump(v) .. ','
    end
    return s .. '} '
  else
    return tostring(o)
  end
end

--
-- Configuration parameters
-- 
local config = {
  -- The minimum and maximum amount a unit is allowed to move before changing
  -- directions. This is to avoid a unit moving in too long a straight line
  -- where artillery can accurately predict where it will be and hit it.
  --
  -- Function takes a unitdefid, returns a number.
  movement_amount_range = memoize(function (udefid)
    local unitdef = UnitDefs[udefid]
    -- A unit with infinite acceleration will have infinitely small movements
    -- (theoretically), likewise a unit with 0 acceleration (i.e. can't change
    -- direction) will have an infinitely long max movement amount.
    --
    -- Calculated by having the unit move at max velocity for 30 to 90 frames.
    return {
      unitdef.speed,
      unitdef.speed * 3
    }
  end),

  -- The maximum number of scramble commands to break a single movement into. Used
  -- as a protection against too many movements.
  --
  -- Function takes a unitdefid, returns a number.
  max_scramble_commands = memoize(function (_)
    return 100
  end),

  -- The max amount the unit is allowed to deviate from the angle between where
  -- it currently is and the eventual target location (in radians).
  --
  -- Function takes a unitdefid, returns a number.
  max_deviation_angle = memoize(function (udefid)
    -- Units with a (hypothetical) infinite turning rate will have a max
    -- deviation angle of math.pi/2 (90 degrees). Units with no ability to turn
    -- (hypothetically) will have a max deviation angle of 0. Under no
    -- circumstances should the deviation angle be greater than the primary
    -- weapon's angle (the unit should always be able to shoot at the target
    -- position).
    local unitdef = UnitDefs[udefid]

    local C = 1298.43 -- Calibrated so the tick deviation angle is ~3π/8
    return math.pi * (1 - math.exp(- unitdef.turnRate/C)) / 2
  end),

  -- Get the command to use while scrambling. Constructors and rez bots should
  -- use the fight command to repair and reclaim units in the area. Combat units
  -- will just move.
  --
  -- Function takes a unitdefid, returns a command.
  get_scramble_cmd_type = memoize(function(unitdefid)
    local unitdef = UnitDefs[unitdefid]
    if unitdef.canResurrect or unitdef.isBuilder or unitdef.canFly then
      return CMD.FIGHT
    end
    return CMD.MOVE
  end)
}


--
-- Local variables
--

-- Scramble command -- arbitrary number
local CMD_SCRAMBLE = 1135

-- Command parameter for move and fight commands that tell us a move or fight
-- command was a part of a scramble command. Arbitrary number.
local MAGIC = 33445

local spGetUnitPosition = Spring.GetUnitPosition
local spGetGroundHeight = Spring.GetGroundHeight

-- Redefinitions to make the linter happier.
local Spring = assert(Spring)
local CMD = assert(CMD)
local CMDTYPE = assert(CMDTYPE)

local scramble_commands = {}
local scramble_commands_id = 0

function widget:CommandsChanged()
  local selectedUnits = Spring.GetSelectedUnits()
  if #selectedUnits > 0 then
    local customCommands = widgetHandler.customCommands
    for i = 1, #selectedUnits do
      local udid = Spring.GetUnitDefID(selectedUnits[i])
      if UnitDefs[udid].canMove or UnitDefs[udid].isFactory then
        customCommands[#customCommands + 1] = {
          id = CMD_SCRAMBLE,
          type = CMDTYPE.ICON_AREA,
          tooltip = 'Randomly path to a point or inside an area.',
          name = 'scramble',
          cursor = 'fight',
          action = 'scramble',
        }
        return
      end
    end
  end
end

-- Returns the final position of a unit after all its commands are executed.
local function getFinalUnitPosition(uid, opts)
  local cmds = Spring.GetCommandQueue(uid, -1)
  if #cmds > 0 and opts.shift and opts.shift == true then
    local c = cmds[#cmds]

    local params = c["params"]

    local x = params[1]
    local y = params[2]
    local z = params[3]

    return x, y, z
  else
    return spGetUnitPosition(uid)
  end
end

local function giveScrambleMovement(uid, ox, oz, r, cmdid, opts)
  local udid = Spring.GetUnitDefID(uid)

  local min_movement_amount, max_movement_amount = unpack(config.movement_amount_range(udid))
  local max_scramble_commands = config.max_scramble_commands(udid)
  local max_deviation_angle = config.max_deviation_angle(udid)

  -- A random point in the circle defined by center (ox, oz) and radius r.
  local theta = math.random() * 2 * math.pi
  local radius = math.random() * r

  local dx = math.cos(theta) * radius
  local dz = math.sin(theta) * radius

  local target_x = ox + dx
  local target_z = oz + dz

  local ux, _, uz = getFinalUnitPosition(uid, opts)

  local points = {}

  -- When scrambling, we want to avoid having each unit move in the same
  -- direction for too long, that way they can dodge artillery. To do this,
  -- if a movement is longer than the MAX_MOVEMENT_AMOUNT, break up the movement
  -- into smaller segments.
  local count = 0
  local cur_x = ux
  local cur_z = uz
  local dist = math.sqrt((cur_x - target_x)^2 + (cur_z - target_z)^2)

  while dist > max_movement_amount and count < max_scramble_commands do
    local angle_to_destination = math.atan2(target_z - cur_z, target_x - cur_x)
    local angle_deviation =
            math.random() * max_deviation_angle * 2 - max_deviation_angle + angle_to_destination
    local len = math.random() * (max_movement_amount - min_movement_amount) + min_movement_amount

    cur_x = cur_x + math.cos(angle_deviation) * len
    cur_z = cur_z + math.sin(angle_deviation) * len

    points[#points + 1] = { cur_x, cur_z }

    count = count + 1
    dist = math.sqrt((cur_x - target_x)^2 + (cur_z - target_z)^2)
  end

  local orders = {}
  for _, p in pairs(points) do
    local x = p[1]
    local z = p[2]
    local y = spGetGroundHeight(x, z)
    orders[#orders + 1] = { cmdid, { x, y, z }, {"shift"} }
  end
  local y = spGetGroundHeight(target_x, target_z)
  opts.shift = 1
  if r == 0 then
    -- The command is just a single click, not an area command. In this case
    -- just move.
    orders[#orders + 1] = { cmdid, { target_x, y, target_z }, { "shift" } }
  else
    -- The MAGIC command is intercepted to repeat if using an area command.
    orders[#orders + 1] = { cmdid, { target_x, y, target_z, MAGIC, ox, oz, r }, { "shift" } }
  end

  Spring.GiveOrderArrayToUnitArray({uid}, orders, --[[ pairwise = ]] false)
end

-- Handler for when a unit completes a command.
function widget:UnitCmdDone(uid, _, _, cmdid, params)
  -- If the command completed is the magic command, this means to continue
  -- scrambling. The parameters for the scrambling are hidden in the last 3
  -- parameters.
  if (cmdid == CMD.MOVE or cmdid == CMD.FIGHT) and #params >= 7 and params[4] == MAGIC then
    local x, z, r = params[5], params[6], params[7]
    giveScrambleMovement(uid, x, z, r, cmdid, {"shift"})
  end
end

-- When it is time for a unit to execute a command.
function widget:UnitCommand(uid, _, _, cmdid, params, options)
  if cmdid == CMD_SCRAMBLE then
    if #params < 4 then
      return
    end

    local cx, _, cz, cr = params[1], params[2], params[3], params[4]

    local udid = Spring.GetUnitDefID(uid)
    if UnitDefs[udid].isFactory == false then
      giveScrambleMovement(uid, cx, cz, cr, config.get_scramble_cmd_type(udid), options)
    end

    return true
  end
end
