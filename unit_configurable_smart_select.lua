function widget:GetInfo()
  return {
    name = "Configurable SmartSelect v0.1",
    desc = "Like SmartSelect by aegis (Ryan Hileman), but with a select-esque DSL",
    author = "UnicodeSnowman, aegis",
    date = "Jul 27, 2023",
    license = "Public Domain",
    layer = -999999,
    enabled = true
  }
end

-- This widget operates as a replacement for smart_select, but is much, much
-- more customizable with a builtin domain-specific language inspired by
-- Spring's builtin 'select' DSL.
--
-- This widget creates an action "selectbox_filter" that takes one argument, the
-- filter chain to apply to the selectbox. The semantics for the filter chain
-- are: if the select box contains units that pass the first filter in the
-- chain, those are selected, otherwise it tries the second filter and so on. In
-- other words, it applies the first filter in the chain for which units are
-- present.
--
-- The filter argument is defined by the informal grammer here, most of which is
-- self explainable (note that tokens are separated by either whitespace and/or
-- underscores and the keywords are case insensitive). This grammar is
-- extensible by adding base filter parsers to the
-- WG.SmartSelect_DSL_BaseFilters.
--
-- FILTER_CHAIN := FILTER
--               | FILTER , FILTER_CHAIN
--
-- FILTER := BASE_FILTER
--         | not BASE_FILTER
--         | FILTER and BASE_FILTER
--         | FILTER or  BASE_FILTER
--         | FILTER xor BASE_FILTER
--
-- BASE_FILTER := ( FILTER )
--              | NUMBER_FILTER
--              | STRING_FILTER
--              | COMMAND_FILTER
--              | BOOLEAN_ATTR
--
-- NUMBER_FILTER := COMPARABLE_NUMBER_ATTR lessthan NUMBER
--                | COMPARABLE_NUMBER_ATTR greaterthan NUMBER
--                | COMPARABLE_NUMBER_ATTR equalto NUMBER
--                | NUMBER_ATTR NUMBER
--
-- STRING_FILTER := STRING_ATTR contains STRING
--                | STRING_ATTR startswith STRING
--                | STRING_ATTR endswith STRING
--                | STRING_ATTR equals STRING
--
-- COMMAND_FILTER := COMMAND_ATTR COMMAND
--
-- STRING_ATTR := name
--              | id
--
-- COMMAND_ATTR := activecommand
--               | commandinqueue
--
-- COMMAND := NUMBER
--          | <command name i.e. guard, patrol, attack>
--
-- COMPARABLE_NUMBER_ATTR := relativehealth
--                         | absolutehealth
--                         | commandqueuesize
--                         | buildrange
--                         | weaponcount
--                         | extractorefficiency
--                         | losradius
--                         | radarradius
--                         | sonarradius
--                         | seismicradius
--                         | targetingpriority
--                         | reclaimspeed
--                         | repairspeed
--                         | maxrepairspeed
--                         | resurrectspeed
--                         | capturespeed
--                         | terraformspeed
--                         | footprintx
--                         | footprintz
--                         | maxvelocity
--                         | maxacceleration
--                         | maxdeceleration
--                         | buildspeed
--                         | buildtime
--                         | buildcostmetal
--                         | buildcostenergy
--                         | health
--
-- NUMBER_ATTR := ingroup
--
-- BOOLEAN_ATTR := all
--               | combat
--               | builder
--               | building
--               | mobile
--               | typeinprevsel
--               | inprevsel
--               | canfly
--               | canassist
--               | canbuild
--               | cancloak
--               | canmove
--               | canreclaim
--               | canrepair
--               | canresurrect
--               | canstockpile
--               | isbuilder
--               | isfactory
--               | extractor
--               | geothermal
--               | canattack
--               | canfight
--               | canpatrol
--               | canguard
--               | canselfdestruct
--               | canrestore
--               | cancapture
--               | canbeassisted
--               | canselfrepair
--               | isairbase
--               | canhover
--               | cansubmerge
--               | canbetransported
--               | cankamikaze
--               | onoffable
--               | hasstealth
--               | hassonarstealth
--               | canmanualfire
--               | isfeatureonbuilt
--               | repairable
--               | capturable
--
-- Some examples (note, underscores and spaces are interchangeable):
--
--    Mobile and RelativeHealth lessthan 50
--    Mobile and RelativeHealth greaterthan 50
--    Mobile and not Name equals armspid
--    Builder_and_CommandQueueSize_equals_0
--    CanFly and not_CommandInQueue_PATROL
--    InGroup 3 or ( InGroup 4 and not CommandInQueue FIGHT )
--
-- Example keybindings for this widget:
--
-- bind          shift+alt  selectbox_filter InPrevSel
-- bind          Any+space  selectbox_filter Mobile_and_RelativeHealth_lessthan_50
-- bind              shift  selectbox_filter All
-- bind           Any+ctrl  selectbox_deselect
-- bind                alt  selectbox_filter TypeInPrevSel
-- bind                  0  selectbox_filter InGroup_0
-- bind                  1  selectbox_filter InGroup_1
-- bind                  2  selectbox_filter InGroup_2
-- bind                  3  selectbox_filter InGroup_3
-- bind                  4  selectbox_filter InGroup_4
-- bind                  5  selectbox_filter InGroup_5
-- bind                  6  selectbox_filter InGroup_6
-- bind                  7  selectbox_filter InGroup_7
-- bind                  8  selectbox_filter InGroup_8
-- bind                  9  selectbox_filter InGroup_9


-- The default filter.
--
-- Selects combat units which are not guarding or patrolling. If none of those
-- are found, it will select all the mobile units. If there are none of those,
-- it will just select anything in the box.
--
-- This filter will also try to not select anything in control-group 0, unless
-- those are the only units in the selection box. This is the control group
-- things like anti-nuke spiders and base guarding units should be in.
local default_filters =
  [[
    Mobile
      and not Builder
      and not ActiveCommand GUARD
      and not CommandInQueue PATROL
      and not CanFly
      and not Name contains armspid
      and not Name contains corvalk
      and not Name contains armatlas
      and not Name contains armdfly
      and not Name contains corseah
      and not InGroup 0,
    CanFly
      and not InGroup 0,
    Mobile
      and not InGroup 0,
    All
      and not InGroup 0,
    All
  ]]

-- local function dump(o)
--   if type(o) == 'table' then
--     local s = '{ '
--     for k,v in pairs(o) do
--       if type(k) ~= 'number' then k = '"'..k..'"' end
--       s = s .. '['..k..'] = ' .. dump(v) .. ','
--     end
--     return s .. '} '
--   else
--     return tostring(o)
--   end
-- end

local function table_empty(tbl)
  for _, _ in pairs(tbl) do
    return false
  end
  return true
end

local minimapToWorld = VFS.Include("luaui/Widgets/Include/minimap_utils.lua").minimapToWorld
local skipSel
local inSelection = false
local inMiniMapSel = false
local spGetUnitHealth = Spring.GetUnitHealth
local spGetUnitGroup       = Spring.GetUnitGroup

local referenceX, referenceY

local includeNanosAsMobile = true

local lastMouseSelection = {}

local spGetMouseState = Spring.GetMouseState
local spGetModKeyState = Spring.GetModKeyState
local spGetSelectionBox = Spring.GetSelectionBox

local spIsGodModeEnabled = Spring.IsGodModeEnabled

local spGetUnitsInScreenRectangle = Spring.GetUnitsInScreenRectangle
local spGetUnitsInRectangle = Spring.GetUnitsInRectangle
local spSelectUnitArray = Spring.SelectUnitArray
local spGetActiveCommand = Spring.GetActiveCommand
local spGetUnitTeam = Spring.GetUnitTeam

local spIsAboveMiniMap = Spring.IsAboveMiniMap

local spGetUnitDefID = Spring.GetUnitDefID
local spGetCommandQueue = Spring.GetCommandQueue
local spGetUnitNoSelect = Spring.GetUnitNoSelect

local GaiaTeamID = Spring.GetGaiaTeamID()
local selectedUnits = Spring.GetSelectedUnits()

local spec = Spring.GetSpectatingState()
local myTeamID = Spring.GetMyTeamID()

local ignore_units = {}
local combat_units = {}
local builder_units = {}
local building_units = {}
local mobile_units = {}

local mod_deselect = false
local mod_append = false

for udid, udef in pairs(UnitDefs) do
  if udef.modCategories['object'] or udef.customParams.objectify then
    ignore_units[udid] = true
  end
  local isMobile = (udef.canMove and udef.speed > 0.000001)  or  (includeNanosAsMobile and (udef.name == "armnanotc" or udef.name == "cornanotc"))
  local builder = (udef.canReclaim and udef.reclaimSpeed > 0)  or  (udef.canResurrect and udef.resurrectSpeed > 0)  or  (udef.canRepair and udef.repairSpeed > 0) or (udef.buildOptions and udef.buildOptions[1])
  local building = (isMobile == false)
  local combat = (not builder) and isMobile and (#udef.weapons > 0)

  if string.find(udef.name, 'armspid') then
    builder = false
  end
  if combat then
    combat_units[udid] = 1
  end

  if builder then
    builder_units[udid] = 1
  end

  if building then
    building_units[udid] = 1
  end

  if isMobile then
    mobile_units[udid] = 1
  end
end
combat_units.name = "Combat Units"

local function just(fn)
  return function (string)
    return fn, string
  end
end

local function read_token(str)
  if not str then
    return nil, ""
  end

  local i = 1
  while i <= #str and (str:sub(i,i):match("%s") or str:sub(i,i) == "_") do
    i = i + 1
  end

  local j = i
  while j <= #str and not (str:sub(j,j):match("%s") or str:sub(j,j) == "_") do
    j = j + 1
  end

  if str:sub(i, j - 1) == "" then
    return nil, ""
  end
  return str:sub(i, j - 1), str:sub(j)
end

-- If the string starts with the token, consume the token and return true.
-- Otherwise don't consume the token and return false.
local function tok(str, token)
  local nexttok, rem = read_token(str)
  if nexttok == token then
    return true, rem
  end
  return false, str
end

-- Parses the next token and passes that token to 'fn' along with the unit.
--
-- fn: (string, unit) -> bool
local function with_string(fn)
  return function (string)
    local token, rem = read_token(string)
    return function (unit)
      return fn(token, unit)
    end, rem
  end
end

-- Returns the value in the dictionary associated with the next token. If the
-- next token does not exist in the dictionary, an error is thrown.
local function select_token(str, dict)
  local token, rem = read_token(str)
  local ret = dict[token]
  if ret == nil then
    local err_msg = "Unexpected token '" .. tostring(token) .. "'. Expecting: "
    local options = {}
    for p, _ in pairs(dict) do
      options[#options + 1] = p
    end
    err_msg = err_msg .. table.concat(options, '/')
    error(err_msg)
  end
  return ret, rem
end

-- fn: Unit -> String
--
-- Returns a parser for a unit filter comparing some string. Creates the
-- sub-language for comparing strings.
local function comparing_string(fn)
  return function(string)
    local cmp = nil
    local rem
    local token

    cmp, rem = select_token(string, {
      ["startswith"] = function (str1, str2) return str1:sub(1, str2:len()) == str2 end,
      ["endswith"] = function (str1, str2) return str1:sub(str1:len() - str2:len()) == str2 end,
      ["contains"] = function(str1, str2) return str1:find(str2) ~= nil end,
      ["equals"] = function(str1, str2) return str1 == str2 ~= nil end
    })

    token, rem = read_token(rem)
    return function(unit)
      local s = fn(unit)
      return s and cmp(s, token)
    end, rem
  end
end

-- fn: UnitDef -> String
local function unitdef_string_filter(fn)
  return comparing_string(function (unit)
    local unitDef = UnitDefs[spGetUnitDefID(unit)]
    if unitDef then
      return fn(unitDef)
    else
      return nil
    end
  end)
end

-- Function is UnitId -> Number
--
-- Parses a comparing function and another number and compares the result of
-- 'fn" to that number.
local function comparing_number(fn)
  return function(string)
    local cmp = nil
    local rem, token

    cmp, rem = select_token(string, {
      ["lessthan"] = function (x, y) return x < y end,
      ["greaterthan"] = function (x, y) return x > y end,
      ["equalto"] = function(x, y) return x == y end,
    })

    token, rem = read_token(rem)
    local num = tonumber(token)
    return function (unit)
      local s = fn(unit)
      return s and num and cmp(s, num)
    end, rem
  end
end

-- Function to parse a filter which takes a comamnd. Passes the command and the
-- unit to 'fn'. The command can either be:
--
-- a. the name of the command i.e. PATROL, ATTACK, GUARD
-- b. the number associated with the command
local function with_command(fn)
  return with_string(function (str, unit)
    local num
    if CMD[str:upper()] then
      num = CMD[str:upper()]
    else
      num = tonumber(str)
    end
    return fn(num, unit)
  end)
end

-- Parses the next token as a number and passes that to the filter.
local function with_number(fn)
  return with_string(function (str, unit)
    return fn(tonumber(str), unit)
  end)
end

-- Parses a filter that just checks if the unitdef is in a dictionary.
local function in_dictionary(dict)
  return just(function (unit)
    local udid = spGetUnitDefID(unit)
    return dict[udid] ~= nil
  end)
end

local function unitdef_bool_filter(fn)
  return just(function (unit)
    local unitDef = UnitDefs[spGetUnitDefID(unit)]
    return unitDef and fn(unitDef)
  end)
end

local function unitdef_number_filter(fn)
  return comparing_number(function (unit)
    local unitDef = UnitDefs[spGetUnitDefID(unit)]
    return fn(unitDef)
  end);
end

local referenceSelection = {}
local referenceSelectionTypes = {}

-- These are all the base filters which can be applied. They can be compounded
-- together using boolean logic operators like AND, OR and NOT.
--
-- A base filter is a function that returns:
--
-- string -> ((unit -> bool), string). In other words, it parses a filter
-- from the provided string.
local default_base_filters = {
  all = just(function () return true end),
  combat = in_dictionary(combat_units),
  builder = in_dictionary(builder_units),
  building = in_dictionary(building_units),
  mobile = in_dictionary(mobile_units),
  typeinprevsel = just(function(unit)
    return referenceSelectionTypes[spGetUnitDefID(unit)] ~= nil
  end),
  inprevsel = just(function(unit)
    return referenceSelection[unit] ~= nil
  end),
  relativehealth = comparing_number(function (unit)
    local health, maxHealth, _, _, _ = spGetUnitHealth(unit)
    return (health / maxHealth) * 100
  end),
  absolutehealth = comparing_number(function (unit)
    local health, _, _, _, _ = spGetUnitHealth(unit)
    return health
  end),
  ingroup = with_number(function (nr, unit)
    return spGetUnitGroup(unit) == nr
  end),
  activecommand = with_command(function (cmd, unit)
    local cq = spGetCommandQueue(unit, 1)
    if cq == nil or #cq == 0 then
      return false
    end
    return cq[1].id == cmd
  end),
  commandinqueue = with_command(function (cmd, unit)
    local cq = spGetCommandQueue(unit, -1)
    for _, c in pairs(cq or {}) do
      if c.id == cmd then
        return true
      end
    end
    return false
  end),
  commandqueuesize = comparing_number(function (unit)
    return spGetCommandQueue(unit, 0)
  end),
  udefid = unitdef_string_filter(function (unitDef) return unitDef.id end),
  name = unitdef_string_filter(function (unitDef) return unitDef.name end),
  canfly = unitdef_bool_filter(function (def) return def.canFly end),
  canassist = unitdef_bool_filter(function (def) return def.canAssist end),
  canbuild = unitdef_bool_filter(function (def) return def.canBuild end),
  cancloak = unitdef_bool_filter(function (def) return def.canCloak end),
  canmove = unitdef_bool_filter(function (def) return def.canMove end),
  canreclaim = unitdef_bool_filter(function (def) return def.canReclaim end),
  canrepair = unitdef_bool_filter(function (def) return def.canRepair end),
  canresurrect = unitdef_bool_filter(function (def) return def.canResurrect end),
  canstockpile = unitdef_bool_filter(function (def) return def.canStockpile end),
  isbuilder = unitdef_bool_filter(function(def) return def.isBuilder end),
  buildrange = unitdef_number_filter(function(def) return def.buildDistance end),
  isfactory = unitdef_bool_filter(function(def) return def.isFactory end),
  weaponcount = unitdef_number_filter(function(def) return #def.weapons end),
  extractor = unitdef_bool_filter(function(def) return def.extractsMetal > 0 end),
  geothermal = unitdef_bool_filter(function(def) return def.needGeo end),
  extractorefficiency = unitdef_number_filter(function(def) return def.extractsMetal end),
  canattack = unitdef_bool_filter(function(def) return def.canAttack end),
  canfight = unitdef_bool_filter(function(def) return def.canFight end),
  canpatrol = unitdef_bool_filter(function(def) return def.canPatrol end),
  canguard = unitdef_bool_filter(function(def) return def.canGuard end),
  canselfdestruct = unitdef_bool_filter(function(def) return def.canSelfDestruct end),
  canrestore = unitdef_bool_filter(function(def) return def.canRestore end),
  cancapture = unitdef_bool_filter(function(def) return def.canCloak end),
  canbeassisted = unitdef_bool_filter(function(def) return def.canBeAssisted end),
  canselfrepair = unitdef_bool_filter(function(def) return def.canSelfRepair end),
  isairbase = unitdef_bool_filter(function(def) return def.customParams.isairbase end),
  canhover = unitdef_bool_filter(function(def) return def.canHover end),
  cansubmerge = unitdef_bool_filter(function(def) return def.canSubmerge end),
  canbetransported = unitdef_bool_filter(function(def) return not def.cantBeTransported end),
  cankamikaze = unitdef_bool_filter(function(def) return def.canKamikaze end),
  onoffable = unitdef_bool_filter(function(def) return def.onOffable end),
  hasstealth = unitdef_bool_filter(function(def) return def.stealth end),
  hassonarstealth = unitdef_bool_filter(function(def) return def.sonarStealth end),
  losradius = unitdef_number_filter(function(def) return def.losRadius end),
  radarradius = unitdef_number_filter(function(def) return def.radarRadius end),
  sonarradius = unitdef_number_filter(function(def) return def.sonarDistance end),
  seismicradius = unitdef_number_filter(function(def) return def.seismicRadius end),
  canmanualfire = unitdef_bool_filter(function(def) return def.canManualFire end),
  isfeatureonbuilt = unitdef_bool_filter(function(def) return def.isFeature end),
  targetingpriority = unitdef_number_filter(function(def) return def.power end),
  repairable = unitdef_bool_filter(function(def) return def.repairable end),
  capturable = unitdef_bool_filter(function(def) return def.capturable end),
  reclaimspeed = unitdef_number_filter(function(def) return def.reclaimSpeed end),
  repairspeed = unitdef_number_filter(function(def) return def.repairSpeed end),
  maxrepairspeed = unitdef_number_filter(function(def) return def.maxRepairSpeed end),
  resurrectspeed = unitdef_number_filter(function(def) return def.resurrectSpeed end),
  capturespeed = unitdef_number_filter(function(def) return def.captureSpeed end),
  terraformspeed = unitdef_number_filter(function(def) return def.terraformSpeed end),
  footprintx = unitdef_number_filter(function(def) return def.footprintX end),
  footprintz = unitdef_number_filter(function(def) return def.footprintZ end),
  maxvelocity = unitdef_number_filter(function(def) return def.speed end),
  maxacceleration = unitdef_number_filter(function(def) return def.maxAcc end),
  maxdeceleration = unitdef_number_filter(function(def) return def.maxDec end),
  buildspeed = unitdef_number_filter(function(def) return def.buildSpeed end),
  buildtime = unitdef_number_filter(function(def) return def.buildTime end),
  buildcostmetal = unitdef_number_filter(function(def) return def.buildCostMetal end),
  buildcostenergy = unitdef_number_filter(function(def) return def.buildCostEnergy end),
  health = unitdef_number_filter(function(def) return def.health end),
}

-- The Widget global SmartSelect_DSL_BaseFilters is pluggable. Other widgets can
-- theoretically add their own base filters to this widget which may then be
-- used in smart_select queries.
if not WG.SmartSelect_DSL_BaseFilters then
  WG.SmartSelect_DSL_BaseFilters = {}
end

for k, v in pairs(default_base_filters) do
  WG.SmartSelect_DSL_BaseFilters[k] = v
end

--
-- Parser Subsystem
--
-- The following functions are for parsing the query string. They don't have
-- anything to do with the selection logic itself.
-- {{{

local function logical_or(filter_1, filter_2)
  return function (unit)
    return filter_1(unit) or filter_2(unit)
  end
end

local function logical_xor(filter_1, filter_2)
  return function (unit)
    local f1 = filter_1(unit)
    local f2 = filter_2(unit)
    return (f1 or f2) and not (f1 and f2)
  end
end

local function logical_and(filter_1, filter_2)
  return function (unit)
    return filter_1(unit) and filter_2(unit)
  end
end

local function logical_not(filter)
  return function (unit)
    return not filter(unit)
  end
end

-- Forward declare parse_filter function
local parse_filter

-- Parse the bottom of the parse filter order of operations. Handles parenthasis
-- and base filter names.
local function parse_filter_1(str)
  local is_paren, filter, token
  local is_not, rem = tok(str, "not")
  if is_not then
    filter, rem = parse_filter_1(rem)
    return logical_not(filter), rem
  else
    is_paren, rem = tok(rem, "(")
    if is_paren then
      filter, rem = parse_filter(rem)
      is_paren, rem = tok(rem, ")")
      if not is_paren then
        error("Missing ')'", 1)
      end
      return filter, rem
    else
      token, rem = read_token(rem)
      local base_filter_parsing_fn = WG.SmartSelect_DSL_BaseFilters[token]
      if base_filter_parsing_fn == nil then
        error("Unrecognized filter '" .. token .. "'")
      end
      filter, rem = base_filter_parsing_fn(rem)
      return filter, rem
    end
  end
end

-- Parses a top-level filter.
local function parse_filter_p(lhs, rem)
  local token, _ = read_token(rem)
  if token == nil or token == ")" then
    return lhs, rem
  end

  local fn
  fn, rem = select_token(rem, {
    ["and"] = logical_and,
    ["or"] = logical_or,
    ["xor"] = logical_xor,
  })

  local rhs
  rhs, rem = parse_filter_1(rem)
  return parse_filter_p(fn(lhs, rhs), rem)
end

parse_filter = function (string)
  local lhs, rem = parse_filter_1(string)
  return parse_filter_p(lhs, rem)
end

-- Like parse_filter, but only returns the filter (not the remaining string) and
-- memoizes the result to avoid reparsing every single time the smart-select is
-- invoked.
local parse_filter_chain_memo_dict = {}
local function parse_filter_chain_memo(str)
  str = str:lower()
  local filter_chain = parse_filter_chain_memo_dict[str]
  if filter_chain then
    return filter_chain
  end
  filter_chain = {}

  for filter_str in str:gmatch('([^,]+)') do
    local filter, _ = parse_filter(filter_str)
    filter_chain[#filter_chain+1] = filter
  end

  parse_filter_chain_memo_dict[str] = filter_chain
  return filter_chain
end

--
-- END Parsing subsection.
-- }}}

local default_filter_chain = parse_filter_chain_memo(default_filters)

local current_filters = {}

-- Handles adding a filter to the list of applied filters.
local function add_filter_handler(_, _, args)
  local str = table.concat(args, ' ')
  current_filters[str] = parse_filter_chain_memo(str)
end

-- Removes a filter from the list of applied filters.
local function remove_filter_handler(_, _, args)
  local str = table.concat(args, ' ')
  current_filters[str] = nil
end

local function set_default_filter_handler(_, _, args)
  WG.SmartSelect_DSL_DefaultFilterChain = table.concat(args, ' ')
  default_filter_chain = parse_filter_chain_memo(WG.SmartSelect_DSL_DefaultFilterChain)
end

local dualScreen
local vpy = select(Spring.GetViewGeometry(), 4)

-- this widget gets called early due to its layer
-- this function will get called after all widgets have had their chance with widget:MousePress
-- function widget:MousePress(x, y, button)
local function mousePress(x, y, button, hasMouseOwner)
  if (hasMouseOwner or button ~= 1) then
    skipSel = true
    return
  end

  skipSel = false

  -- Store which types are in the reference selection. This is used for the
  -- "same" filter ... the filter that selects only the types of units already
  -- selected.
  referenceSelection = selectedUnits
  referenceSelectionTypes = {}
  for i = 1, #referenceSelection do
    local udid = spGetUnitDefID(referenceSelection[i])
    if udid then
      referenceSelectionTypes[udid] = 1
    end
  end

  inMiniMapSel = spIsAboveMiniMap(x, y)
  if inMiniMapSel then
    referenceX, _, referenceY = minimapToWorld(x, y, vpy, dualScreen)
  end
end

local function set_deselect(_, _, _, data)
  mod_deselect = data[1]
end

local function set_append(_, _, _, data)
  mod_append = data[1]
end

function widget:Initialize()
  WG.SmartSelect_MousePress2 = mousePress
  if not WG.SmartSelect_DSL_DefaultFilterChain then
    WG.SmartSelect_DSL_DefaultFilterChain = default_filters
  end
  widgetHandler:AddAction("selectbox_filter", add_filter_handler, nil, "p")
  widgetHandler:AddAction("selectbox_filter", remove_filter_handler, nil, "r")
  widgetHandler:AddAction("selectbox_deselect", set_deselect, { true }, "p")
  widgetHandler:AddAction("selectbox_deselect", set_deselect, { false }, "r")
  widgetHandler:AddAction("selectbox_append", set_append, { true }, "p")
  widgetHandler:AddAction("selectbox_append", set_append, { false }, "r")
  widgetHandler:AddAction("default_select_filter", set_default_filter_handler, nil, "p")
  widget:ViewResize()
end

local function sort(v1, v2)
  if v1 > v2 then
    return v2, v1
  else
    return v1, v2
  end
end

local function GetUnitsInMinimapRectangle(x, y)
  local left = referenceX
  local top = referenceY
  local right, _, bottom = minimapToWorld(x, y, vpy, dualScreen)

  left, right = sort(left, right)
  bottom, top = sort(bottom, top)

  return spGetUnitsInRectangle(left, bottom, right, top)
end

function widget:ViewResize()
  dualScreen = Spring.GetMiniMapDualScreen()
  _, _, _, vpy = Spring.GetViewGeometry()
end

function widget:SelectionChanged(sel)
  -- Check if engine has just deselected via mouserelease on selectbox.
  -- We want to ignore engine passed selection and make sure we retain smartselect state
  if inSelection and not select(3, spGetMouseState()) then -- left mouse button
    inSelection = false

    if #sel == 0 and not select(2, spGetModKeyState()) then -- ctrl
      -- if empty selection box and engine hardcoded deselect modifier is not
      -- pressed, user is selected empty space
      -- we must clear selection to disambiguate from our own deselect modifier
      selectedUnits = {}
      spSelectUnitArray({})
    else
      -- we also want to override back from engine selection to our selection
      spSelectUnitArray(selectedUnits)
    end
    return selectedUnits
  end

  selectedUnits = sel
end

function widget:PlayerChanged()
  spec = Spring.GetSpectatingState()
  myTeamID = Spring.GetMyTeamID()
end

function widget:Update()
  if skipSel or spGetActiveCommand() ~= 0 then
    return
  end

  local x, y, lmb = Spring.GetMouseState()
  if lmb == false then inMiniMapSel = false end

  -- get all units within selection rectangle
  local x1, y1, x2, y2 = spGetSelectionBox()

  inSelection = inMiniMapSel or (x1 ~= nil)
  if not inSelection then return end -- not in valid selection box (mouserelease/minimum threshold/chorded/etc)

  local mouseSelection
  if inMiniMapSel then
    mouseSelection = GetUnitsInMinimapRectangle(x, y)
  else
    mouseSelection = spGetUnitsInScreenRectangle(x1, y1, x2, y2, nil) or {}
  end

  local newSelection = {}
  local uid, tmp

  tmp = {}
  local n = 0
  local equalsMouseSelection = #mouseSelection == #lastMouseSelection
  local isGodMode = spIsGodModeEnabled()

  -- Filter out units in the mouse selection that are not controllable like gaia
  -- units and units deemed to be ignored.
  for i = 1, #mouseSelection do
    uid = mouseSelection[i]
    if not spGetUnitNoSelect(uid) and -- filter unselectable units
      (isGodMode or (spGetUnitTeam(uid) ~= GaiaTeamID and not ignore_units[spGetUnitDefID(uid)] and (spec or spGetUnitTeam(uid) == myTeamID))) then -- filter gaia units + ignored units (objects) + only own units when not spectating
      n = n + 1
      tmp[n] = uid
      if equalsMouseSelection and not lastMouseSelection[uid] then
        equalsMouseSelection = false
      end
    end
  end

  -- Fill dictionary for set comparison
  -- We increase slightly the perf cost of cache misses but at the same
  -- we hit caches way more often. At thousands of units, this improvement
  -- is massive
  lastMouseSelection = {}
  for i = 1, #mouseSelection do
    lastMouseSelection[mouseSelection[i]] = true
  end
  mouseSelection = tmp

  local filter_chains
  if table_empty(current_filters) then
    filter_chains = { default_filter_chain }
  else
    filter_chains = current_filters
  end


  -- Get all the units which pass the filter.
  local unfiltered_units = {}
  for _, filter_chain in pairs(filter_chains) do
    local unfiltered_units_for_this = {}
    local i = 1

    while #unfiltered_units_for_this == 0 and i <= #filter_chain do
      for j = 1, #mouseSelection do
        uid = mouseSelection[j]
        if not unfiltered_units[uid] and filter_chain[i](uid) then
          unfiltered_units_for_this[#unfiltered_units_for_this+1] = uid
        end
      end
      i = i + 1
    end

    for _, uid in pairs(unfiltered_units_for_this) do
      unfiltered_units[uid] = 1
    end
  end

  if mod_deselect then
    for _, u in pairs(referenceSelection) do
      if not unfiltered_units[u] then
        newSelection[#newSelection+1] = u
      end
    end
  else
    if mod_append then
      for uid, _ in pairs(selectedUnits) do
        newSelection[#newSelection + 1] = uid
      end
    end

    for uid, _ in pairs(unfiltered_units) do
      newSelection[#newSelection+1] = uid
    end
  end

  spSelectUnitArray(newSelection)
end

--- Profiling Update, remember to change widget:Update to local function update above
--
--local spGetTimer = Spring.GetTimer
--local highres = nil
--if Spring.GetTimerMicros and  Spring.GetConfigInt("UseHighResTimer", 0) == 1 then
--  spGetTimer = Spring.GetTimerMicros
--  highres = true
--end
--
--function widget:Update()
--  local sTimer = spGetTimer()
--
--  update()
--
--  Spring.Echo('Update time:', Spring.DiffTimers(spGetTimer(), sTimer, nil, highres))
--end
--
function widget:Shutdown()
  WG.SmartSelect_MousePress2 = nil
end

