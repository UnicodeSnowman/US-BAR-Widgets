function widget:GetInfo()
  return {
    name = "DisengageDamagedUnits",
    desc = "Disengages damaged units from the battle",
    author = "UnicodeSnowman",
    date = "May 2023",
    license = "GPL v2",
    layer = 0,
    enabled = true
  }
end

-- Some defines to make the language server happier.
widgetHandler = assert(widgetHandler)
widget = assert(widget)
Spring = assert(Spring)
WG = assert(WG)

-- Units below this damage threshold will be disengaged. TODO make this
-- configurable.
local damageThreshold = 0.45

-- Spring commands for more speed.
local spGetSelectedUnits = Spring.GetSelectedUnits
local spGetUnitHealth = Spring.GetUnitHealth
local spGiveOrderToUnit = Spring.GiveOrderToUnit
local spSelectUnitArray = Spring.SelectUnitArray

local function disengageDamagedUnits()
  local newSelection = {}

  for _, uid in pairs(spGetSelectedUnits()) do
    local health, maxHealth, _, _, _ = spGetUnitHealth(uid)
    local percent = health / maxHealth

    if percent < damageThreshold then
      spGiveOrderToUnit(uid, WG.CMD_DISENGAGE, {}, 0)
    else
      table.insert(newSelection, uid)
    end
  end

  spSelectUnitArray(newSelection)
end

function widget:Initialize()
  -- The units with health below the health threshold will be disegaged from
  -- combat and be sent back to the fallback position.
  widgetHandler:AddAction(
    "disengage_damaged_units", disengageDamagedUnits, nil, "p")
end

